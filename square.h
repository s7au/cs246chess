#ifndef __SQUARE_H__
#define __SQUARE_H__

class Board;
class Piece;

class Square{
private:
	Piece *occupant; // Pointer to a piece occupying the square, if any
	bool blackTargets; // Indicates if square is marked by black pieces
	bool whiteTargets; // Indicates if square if marked by white pieces
	
    int x; // The row that the square is on
	int y; // The column that the square is on
	Board *game; // The board that the square is on
	
public:
	Piece *getOccupant(); // Returns occupant pointer
	void setSquare(int x, int y); // Set square's coordinates to x and y of board
	void setPiece(Piece *p); // Sets occupant pointer
	void removePiece(); // Sets occupant pointer to NULL (note this does not delete the piece)

    Square();

	// gets coordinate of square
	int getX(); // gets x-coordinate
	int getY(); // gets y-coordinate

	// sets Square to be targetable by black or white
	void setTarget(int color);

	// returns square targetability based on input color
	bool getTarget(int color);

	// clears square targetability
	void clearTarget();

	// sets board pointers
	void setBoard(Board* master);

};

#endif
