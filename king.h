#ifndef __KING_H__
#define __KING_H__

#include "piece.h"
#include <vector>
#include "move.h"

struct Move;

using namespace std;

class King:public Piece{
	bool moved; // Indicates if King has moved 
public:
	bool getMoved(); // Returns moved boolean
	vector<Move> possibleMoves();
	King(Square* position, int color);
};

#endif
