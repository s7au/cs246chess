#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include <iostream>
#include "board.h"
#include "view.h"
#include "player.h"
#include <string>

struct Move;

class Controller {

	// pointers to AI - will implement later
	Player *pW; // opponent if White
	Player *pB; // opponent if Black
	
	View *td; // The text display.
	View *gd; // The graphic display.
	
	Board *theBoard; // Pointer to Board Model

	int playerTurn; // used to track whose turn
	double blackScore; // black player's score
	double whiteScore; // white player's score

	/* 
	 * whitePlayer and blackPlayer used to denote where input will be from
	 * 0 - from stdin
	 * 1-4 - AI level 1-4
	 */
	int whitePlayer;
	int blackPlayer;

	/*
	 * Converts input of white and black player identities
	 * Can handle empty strings
	 * Expects bw to be "human" or "computer[1-4]"
	 * If human will output 0
	 * If computer will be 1-4
	 * If anything else it will be invalid and will output -1
	 */
	int inputToPlayer(std::string bw);

	void printScore(); // prints current score

  public:
	Controller();
	~Controller();

	// asks player for choice to promote pawn
	// can only promote to 'q' - queen 'r' - rook 'n' - knight or 'b' - bishop
	char askPawn(int color);

	/*
	 * Function to start game play
	 * graphics: true is program is using program component, false otherwise
	 * file: if not an empty string will contain a file to setup custom game
	 * Will check if game is valid
	 */
	void play(bool graphics = 0, std::string file = "");

	// updates the View with new game state
	void notify(Move turn);

	/**
	 * Same as above but specific to adding pieces to board
	 * @param x - x coordinate of piece
	 * @param y - y coordinate of piece
	 * @param occupant - type of piece (king/rook etc.)
	 */
	void notify(int x, int y, char occupant); // add this to UML

};

#endif
