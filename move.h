#ifndef __MOVE_H__
#define __MOVE_H__

#include <iostream>

/* Purpose: Will use this structure to pass chess moves to functions
 * All boards will be 2D arrays; for example theBoard in view will be
 * an array of arrays where each inner array corresponds to a column.
 * This is to ensure the column-row notation given by chess moves is not
 * confused when determining board position.
 *
 * Example: "b8" in theBoard will be theBoard[1][7]
 */
struct Move {
	int xi,yi,xf,yf;
	Move(const std::string &init, const std::string &fin);
	Move(const int &xf, const int &yf, const int &xi, const int &yi);
	Move(const Move& other);
	Move &operator=(const Move& other);
};

std::ostream &operator<<(std::ostream &out, const Move &m);

#endif
