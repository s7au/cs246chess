#include <vector>
#include <iostream>
#include "controller.h"
#include "square.h"
#include "board.h"
#include "move.h"
#include "piece.h"
#include "king.h"
#include "bishop.h"
#include "queen.h"
#include "pawn.h"
#include "knight.h"
#include "rook.h"
using namespace std;

//  *Board::boardInstance = NULL; //initialized to NUll so instance can be created - no longer used

Board::Board() {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].setSquare(i, j);
		}
	}
	//theBoard[0][0].setBoard();
}

void Board::setupBoard(Board &other) {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].setBoard(this);
			Piece *temp = other.theBoard[i][j].getOccupant();
			if (temp) {
				if (temp->getColor()) {
					setupPiece(i, j, temp->getName());
				} else {
					setupPiece(i, j, temp->getName() - 'a' +'A');
				}
			}
		}
	}
}



void Board::setupBoard() {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].setBoard(this);
		}
	}
}


void Board::clearGame() {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].removePiece();
			theBoard[i][j].clearTarget();
		}
	}
	// THIS WILL BE WHERE YOU DELETE PIECES IN PIECE LIST!!!! CHECK VECTOR BEHAVIOR
	
}

bool Board::isValid(Move turn, int color) {
	Piece *relevantPiece = theBoard[turn.xi][turn.yi].getOccupant();
	if (relevantPiece && relevantPiece->getColor() == color) {
		vector<Move> pieceMoves = relevantPiece->possibleMoves();
		for (vector<Move>::iterator i = pieceMoves.begin(); i != pieceMoves.end(); i++) {
			if (turn.xf == i->xf && turn.yf == i->yf) return true;
		}
	}
	return false;
}
	
bool Board::boardFinished() {
	if (!whitePieces.empty() && !blackPieces.empty() && 
	(*whitePieces.begin())->getName() == 'k' && (*blackPieces.begin())->getName() == 'k' &&
	!checkCheck(0) && !checkCheck(1)) {
		return true;
	} else {
		return false;
	}
} // checks if boar setup is valid

void Board::init(bool normal) {
	clearGame();
	if (normal) {
		setupPiece(0,7,'r');
		setupPiece(1,7,'n');
		setupPiece(2,7,'b');
		setupPiece(3,7,'q');
		setupPiece(4,7,'k');
		setupPiece(5,7,'b');
		setupPiece(6,7,'n');
		setupPiece(7,7,'r');
		setupPiece(0,0,'R');
		setupPiece(1,0,'N');
		setupPiece(2,0,'B');
		setupPiece(3,0,'Q');
		setupPiece(4,0,'K');
		setupPiece(5,0,'B');
		setupPiece(6,0,'N');
		setupPiece(7,0,'R');
		for (int i=0; i<8; i++) {
			setupPiece(i,6,'p');
		}
		for (int i=0; i<8; i++) {
			setupPiece(i,1,'P');
		}
	}
}

void Board::setController(Controller *boss) {
	this->boss = boss;
}

// no longer in use
// Board *Board::getBoard() {
// 	if (!boardInstance) {
// 		boardInstance = new Board;
// 	}
// 	return boardInstance;
// }

void Board::reverseMove(Move turn, Board &original) {
	Piece *temp = theBoard[turn.xf][turn.yf].getOccupant();
	theBoard[turn.xf][turn.yf].removePiece();
	theBoard[turn.xi][turn.yi].setPiece(temp);
	Piece *tempDead = original.theBoard[turn.xf][turn.yf].getOccupant();
	if (tempDead) {
		if (tempDead->getColor()) {
			setupPiece(turn.xf, turn.yf, tempDead->getName());
		} else {
			setupPiece(turn.xf, turn.yf, tempDead->getName() - 'a' +'A');
		}
	}
}


bool Board::movePiece(Move turn, int color) {

	if (isValid(turn, color)) { // Handles castling, confirmed working
		if(theBoard[turn.xi][turn.yi].getOccupant()->getName() == 'k' && !(theBoard[turn.xi][turn.yi].getOccupant()->getMoved())){
			if(turn.xf == 6){
				Piece *rookptr = theBoard[7][turn.yi].getOccupant();
				Move rookturn (5, turn.yi, 7, turn.yi);
				theBoard[7][turn.yi].removePiece();
				theBoard[5][turn.yf].setPiece(rookptr);
				boss->notify(rookturn);
			}
			if(turn.xf == 2){
				Piece *rookptr = theBoard[0][turn.yi].getOccupant();
				Move rookturn (3, turn.yi, 0, turn.yi);
				theBoard[3][turn.yi].removePiece();
				theBoard[3][turn.yf].setPiece(rookptr);
				boss->notify(rookturn);
			}
		}
		
		if(theBoard[turn.xi][turn.yi].getOccupant()->getName() == 'p' && (turn.xi != turn.xf) && theBoard[turn.xi][turn.yi].getOccupant()->enPassant()){
			theBoard[turn.xf][turn.yi].removePiece(); // Handles enpassant
			char blank;
			if ((xf+turn.yi)%2) blank = ' ';
			else blank = '_';
			boss->notify(turn.xf, turn.yi, blank);
		}
		
		theBoard[turn.xf][turn.yf].removePiece();
		Piece *temp = theBoard[turn.xi][turn.yi].getOccupant();
		theBoard[turn.xi][turn.yi].removePiece();
		theBoard[turn.xf][turn.yf].setPiece(temp);
		boss->notify(turn);
		
		if(theBoard[turn.xi][turn.yf].getOccupant()->getName() == 'p' && (turn.yf == 0 || turn.yf == 7)){
			theBoard[turn.xf][turn.yf].removePiece(); //Handles promotion, not sure how cin should be implemented here
			char type;
			cin >> type;
			while(type != 'r' && type != 'k' && type != 'b' && type != 'q' &&
				  type != 'R' && type != 'K' && type != 'B' && type != 'Q' ){
				cin >> type;
			}
			setupPiece(turn.xf, turn.yf, type);
			boss->notify(turn.xf, turn.yf, type);
		}
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				theBoard[i][j].clearTarget();
			}
		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				theBoard[i][j].clearTarget();
			}
		}
		if (color) {
			for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
				if ((*i)->getName() == 'p') {
					dynamic_cast<Pawn*>(*i)->setDashed(false);
				}
			}
		} else {
			for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
				if ((*i)->getName() == 'p') {
					dynamic_cast<Pawn*>(*i)->setDashed(false);
				}
			}
		}
		return true;
	} else {
		return false;
	}
}

void Board::setupPiece(int x, int y, char piece) {
	Piece *newP;
	int color;
	if (piece < 'a') color = 0 ;
	else color = 1;
	if (theBoard[x][y].getOccupant()) {
		theBoard[x][y].removePiece();
	}
	if (piece == 'k' || piece == 'K') {
		newP = new King(&theBoard[x][y], color);
		if (color) blackPieces.insert(blackPieces.begin(), newP);
		else whitePieces.insert(whitePieces.begin(), newP);
	} else if (piece == 'q' || piece == 'Q') {
		newP = new Queen(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	} else if (piece == 'n' || piece == 'N') {
		newP = new Knight(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	} else if (piece == 'b' || piece == 'B') {
		newP = new Bishop(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	} else if (piece == 'r' || piece == 'R') {
		newP = new Rook(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	}else if (piece == 'p' || piece == 'P') {
		newP = new Pawn(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	}
	theBoard[x][y].setPiece(newP);
	newP->setBoard(this);
}

bool Board::removePiece(int x, int y) {
	if (theBoard[x][y].getOccupant()) {
		theBoard[x][y].removePiece();
		return true;
	} else return false;
}

bool Board::checkCheck(int turn) {
	if (turn) {
		for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
			(*i)->possibleMoves();
		}
		if ((*blackPieces.begin())->getSquare()->getTarget(0)) {
			return true;
		} else return false;
	} else {
		for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
			(*i)->possibleMoves();
		}
		if ((*whitePieces.begin())->getSquare()->getTarget(1)) {
			return true;
		} else return false;
	}
}

// I NEED TO FIGURE THIS OUTTTTT!!!!!!
// check all moves of current
// calculate targeting of otherside
// checkcheck current
bool Board::checkWin(int turn) {
	Board *temp = new Board;
	temp->setupBoard(*this);
	vector<Move> moveList;
	if (turn) {
		for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	} else {
		for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	}
	for (vector<Move>::iterator i = moveList.begin(); i != moveList.end(); i++) {
		temp->movePiece(*i, turn);
		temp->checkCheck((turn+1)%2);
		if (temp->checkCheck(turn)) {
			temp->reverseMove(*i, *this);
		} else {
			delete temp;
			return false;
		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				temp->theBoard[i][j].clearTarget();
			}
		}
	}
	delete temp;
	return true;
}

bool Board::checkStale(int turn) {
	if (turn) {
		for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
			if (!(*i)->possibleMoves().empty()) return false;
		}
	} else {
		for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
			if (!(*i)->possibleMoves().empty()) return false;
		}
	}
	return true;
}
