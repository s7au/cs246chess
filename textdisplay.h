#ifndef __TEXTDISPLAY_H__
#define __TEXTDISPLAY_H__
#include <iostream>
#include <sstream>
#include "view.h"

class TextDisplay : public View {
 public:
  TextDisplay(bool normal);
  ~TextDisplay(); //dtor
  /* 
   * Purpose: prints current board state to screen when there is no
   * graphic display
   * Numbering and position is in accordance with general chess notation
   * White spaces are denoted by blank spaces while black spaces are denoted
   * by underscores. Pieces denoted by letters  generally by the first letter
   * of each piece name with the exception of knights being denoted 'n'. 
   * Uppercase for white pieces and lowercase for black pieces
   */
  void print(std::ostream &out);

};

#endif
