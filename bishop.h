#ifndef __BISHOP_H__
#define __BISHOP_H__

#include <vector>
#include "piece.h"

struct Move;

class Bishop:public Piece{
public:
	std::vector<Move> possibleMoves();
	Bishop(Square* position, int color);
};

#endif
