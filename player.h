#ifndef __PLAYER_H_
#define __PLAYER_H_

struct Move;

class Player {
	public:
		// returns move of player - more details in subclasses
		virtual Move getMove() = 0;
		virtual ~Player() = 0;
};

#endif
