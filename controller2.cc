#include <iostream>
#include <stdlib.h>
#include <fstream>
#include "controller.h"
#include "view.h"
#include "board.h"
#include "textdisplay.h"
#include "graphicsdisplay.h"
#include "move.h"
#include "human.h"
#include "ai.h"
#include "lvl1.h"
#include "lvl2.h"
<<<<<<< HEAD
//#include "lvl3.h"
=======
#include "lvl3.h"
#include <unistd.h>
>>>>>>> c0d26c25b823fb059b4d2c04af3e8a2a3c0ee0f8
using namespace std;

Controller::Controller() : pW(NULL), pB(NULL), td(NULL), gd(NULL), blackScore(0), whiteScore(0) {
	theBoard = new Board();
	theBoard->setupBoard();
	theBoard->setController(this);
}

void Controller::notify(Move turn){
	if (td) td->notify(turn);
	if (gd) gd->notify(turn);
}

void Controller::notify(int x, int y, char occupant) {
	if (td) td->notify(x,y,occupant);
	if (gd) gd->notify(x,y,occupant);
}

char Controller::askPawn(int color) {
	char type;
	if ((color && blackPlayer) || (!color && whitePlayer)) {
		return 'q';
	} else {
		cin >> type;
		while(type != 'r' && type != 'k' && type != 'b' && type != 'q' &&
			  type != 'R' && type != 'K' && type != 'B' && type != 'Q' ){
			cout << "Invalid promotion" << endl;
			cin >> type;
		}
		return type;
	}
}

int Controller::inputToPlayer(string bw) {
	if (bw == "human") return 0;
	else {
		stringstream ss(bw);
		char c;
		while (ss >> c) {
			if (c < '5' && c > '0') {
				return c - '0';
			}
		}
		cin.clear();
		return -1;
	}
}

void Controller::play(bool graphics, string file){
	srand(time(NULL));
	string cmd;
	bool gameInProgress = false; // true if game is in progress, false otherwise
	bool usingCustomGame = false; // true if unique game is used

    if(file != "") { // Loading a pre-saved game
        ifstream input(file.c_str());
            delete td;
            delete gd;
            if (graphics) gd = new GraphicsDisplay(false);
            else td = new TextDisplay(false);
            playerTurn = 0;
            theBoard->init(false); // clears board
        
            for(int y = 7; y > -1; y--){
                for(int x = 0; x < 8; x++){
                    char piece;
                    input >> piece;
                    if(piece != '_'){
                        theBoard->setupPiece(x, y, piece);
                        notify(x, y, piece);
                        if (td) td->print(cout);
                        if (gd) gd->update();
                    }
                }
            }
            
            char col; // color recorded in doc
            input >> col;
            if(col == 'w' || col == 'W'){
                playerTurn = 0;
            }
            else if(col == 'b' || col == 'B'){
                playerTurn = 1;
            }

            pW = new Human();
            pB = new Human();

            gameInProgress = true;
            usingCustomGame = true;
                    
    }

	while (cin >> cmd) {
		if (cmd == "game") {
			gameInProgress = true;
			playerInCheck = false;

			//first we will deal with players
			delete pW;
			delete pB;

			string white, black;
			cin >> white >> black;
			whitePlayer = inputToPlayer(white);
			blackPlayer = inputToPlayer(black);
			if (whitePlayer == -1 || blackPlayer == -1) {
				cerr << "Invalid player choices!" << endl;
				gameInProgress = false;
				continue;
			}
			// pW = new Human();
			// pB = new Human();
			// PLEASE UNCOMMENT WHEN IMPLEMENTING AI

			switch(whitePlayer) {
				case 0: pW = new Human(); break;
				case 1: pW = new Lvl1(0, theBoard); break;
				case 2: pW = new Lvl2(0, theBoard); break;
			//	case 3: pW = new Lvl3(0, theBoard); break;
			}
			switch(blackPlayer) {
				case 0: pB = new Human(); break;
				case 1: pB = new Lvl1(1, theBoard); break;
				case 2: pB = new Lvl2(1, theBoard); break;
			//	case 3: pB = new Lvl3(1, theBoard); break;
			}

			/*
			 * next we will deal with board initialization
			 * We only need to bother with this if setup isn't called before
			 */
			if (!usingCustomGame) {
				delete td;
				delete gd;
				if (graphics) gd = new GraphicsDisplay(true);
				else td = new TextDisplay(true);
				theBoard->init(true);
				playerTurn = 0;
			}
			if (td) td->print(cout);
			if (gd) gd->update();

		} else if (cmd == "resign"){
			if (gameInProgress) {
				if ((playerTurn)%2) whiteScore++;
				else blackScore++;
				usingCustomGame = false;
				cout << "Current Score:" << endl;
				cout << "White: " << whiteScore << endl;
				cout << "Black: " << blackScore << endl;
			} else { // in case resign is called when game not in progress
				cerr << "No game in progress!" << endl;
			}
			gameInProgress = false;

		} else if (cmd == "move") {
			// usleep(1500000); if you want to see moves happen
			if (!gameInProgress) { // in case game not in progress
				cerr << "No game in progress!" << endl;
				continue;
			}
			bool validMove; // takes in whether move was implemented based on validity

            Move next(0, 0, 0, 0);

<<<<<<< HEAD
            Move test(0,0,0,0); // REMOVE THIS LATER
    
      //      cout << "Move validation beginning" << endl; // DEBUG ////////

=======
>>>>>>> c0d26c25b823fb059b4d2c04af3e8a2a3c0ee0f8
			if (playerTurn%2) {
				validMove = theBoard->movePiece(pB->getMove(), 1, playerInCheck);
			} else {
<<<<<<< HEAD
      //          cout << "Getting moves" << endl; // DEBUG ////////////
				test = pW->getMove();
        //        cout << "Move get!" << endl; // DEBUG //////
				validMove = theBoard->movePiece(test, 0, playerInCheck);
=======
				validMove = theBoard->movePiece(pW->getMove(), 0, playerInCheck);
>>>>>>> c0d26c25b823fb059b4d2c04af3e8a2a3c0ee0f8
			}
            
          //  cout << "Move validation complete" << endl; // DEBUG /////////////

			if (validMove) {
				if (td) td->print(cout);
				if (gd) gd->update();
				playerTurn++;
				int colorTurn = playerTurn%2; // so calculation is not done multiple times
				if (theBoard->checkCheck(colorTurn)) {
					if (theBoard->checkWin(colorTurn)) {
						if (colorTurn) {
							cout << "White wins!" << endl;
							whiteScore++;
							gameInProgress = false;
						} else {
							cout << "Black wins!" << endl;
							blackScore++;
							gameInProgress = false;
						}
					} else {
						playerInCheck = true;
						if (colorTurn) cout << "Black is in check" << endl;
						else cout << "White is in check" << endl;
					}
				} else {
					if (theBoard->checkStale(colorTurn)) {
						cout << "Stalemate" << endl;
						gameInProgress = false;
						blackScore += 0.5;
						whiteScore += 0.5;
					}
					playerInCheck = false;
				}
			} else {
				cout << "Invalid Move" << endl;
			}

		} else if (cmd == "setup") {
			if (gameInProgress) {
				cout << "Game in progress" << endl;
				continue;
			}
			string input;
			string space;
			
			delete td;
			delete gd;
			if (graphics) gd = new GraphicsDisplay(false);
			else td = new TextDisplay(false);
			playerTurn = 0;
			theBoard->init(false); // clears board
			
			while (cin >> input) {
				if (input == "+") {
					string piece;
					cin >> piece >> space;
					theBoard->setupPiece(space[0]-'a', space[1]-'1', piece[0]);
					notify(space[0]-'a', space[1]-'1', piece[0]);
					if (td) td->print(cout);
					if (gd) gd->update();

				} else if (input == "-") {
					cin >> space;
					notify(space[0]-'a', space[1]-'1', 0);
					if (theBoard->removePiece(space[0]-'a', space[1]-'1')) {
						if (td) td->print(cout);
						if (gd) gd->update();
					}

				} else if (input == "=") {
					string colour;
					cin >> colour;
					if (colour == "white") {
						playerTurn = 0;
					} else if (colour == "black") {
						playerTurn = 1;
					} else {
						cerr << "Please choose a valid colour!";
					}

				} else if (input == "done") {
					break;
				} else {
					cerr << "Invalid command!";
				}
			}
			if (theBoard->boardFinished()) {
				usingCustomGame = true;
				cout << "Setup complete" << endl;
			} else {
				cout << "Setup incomplete" << endl;
			}
		}
		if (gameInProgress) {
			if (playerTurn%2) cout << "Black's turn" << endl;
			else cout << "White's turn" << endl;
		}  
	}
	cout << "Final Score:" << endl;
	cout << "White: " << whiteScore << endl;
	cout << "Black: " << blackScore << endl;
}

Controller::~Controller(){
	delete td;
	delete gd;
	delete theBoard;
	delete pW;
	delete pB;
}
