#include <iostream>
#include "square.h"
#include "board.h"
#include "piece.h"

Piece *Square::getOccupant(){
	return occupant;
}

void Square::setSquare(int x, int y){
	this->x = x;
	this->y = y;
}

void Square::setPiece(Piece *p){
	this->occupant = p;
	p->setSquare(this);
}

void Square::removePiece(){
	if (this->occupant) {
		occupant->setSquare(NULL);
		this->occupant = NULL;
	}
}

int Square::getX() {
	return x;
}

int Square::getY() {
	return y;
}

void Square::setTarget(int color) {
	if (color) blackTargets = true;
	else whiteTargets = true;
}

bool Square::getTarget(int color) {
	if (color) return blackTargets;
	else return whiteTargets;
}

void Square::clearTarget() {
	blackTargets = false;
	whiteTargets = false;
}

Square::Square() : occupant(NULL), blackTargets(false), whiteTargets(false) {}

// void Square::setBoard() {
// 	game = Board::getBoard();
// }
// 
void Square::setBoard(Board* master) {
	game = master;
}
