#ifndef __LVL2_H__
#define __LVL2_H__
#include "ai.h"
#include <vector>

struct Move;

class Lvl2 : public AI {
public:
	Move getMove(); // prioritizes capturing and checking
	Lvl2();
	Lvl2(int player, Board *mainBoard); 
};

#endif
