#include "rook.h"
#include <vector>
#include "move.h"

using namespace std;

Rook::Rook(Square* position, int color) : Piece(position, color) {
	name = 'r';
	moved = false;
}

bool Rook::getMoved(){
	return this->moved;
}

vector<Move> Rook::possibleMoves(){
     vector<Move> moveList;

     if(position == NULL) return moveList;     

     movesInDirection(&moveList, 1, 0);
     movesInDirection(&moveList, 0, 1);
     movesInDirection(&moveList, -1, 0);
     movesInDirection(&moveList, 0, -1);
     return moveList;           
}
