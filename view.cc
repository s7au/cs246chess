#include "view.h"
#include "move.h"
using namespace std;

View::View(bool normal) {
	for (int i=0; i<8; i++) {
		for (int j=0; j<8; j++) {
			if ((i+j)%2) {
				theBoard[j][i] = ' ';
			} else {
				theBoard[j][i] = '_';
			}
		}
	}
	if (normal) {
		theBoard[0][7] = 'r';
		theBoard[1][7] = 'n';
		theBoard[2][7] = 'b';
		theBoard[3][7] = 'q';
		theBoard[4][7] = 'k';
		theBoard[5][7] = 'b';
		theBoard[6][7] = 'n';
		theBoard[7][7] = 'r';
		theBoard[0][0] = 'R';
		theBoard[1][0] = 'N';
		theBoard[2][0] = 'B';
		theBoard[3][0] = 'Q';
		theBoard[4][0] = 'K';
		theBoard[5][0] = 'B';
		theBoard[6][0] = 'N';
		theBoard[7][0] = 'R';
		for (int i=0; i<8; i++) {
			theBoard[i][6] = 'p';
		}
		for (int i=0; i<8; i++) {
			theBoard[i][1] = 'P';
		}
	}
}

void View::notify(Move turn) {
	theBoard[turn.xf][turn.yf] = theBoard[turn.xi][turn.yi];
	// checkerboard pattern - sum of dimension '_' if even, ' ' if odd
	if ((turn.xi + turn.yi)%2) {
		theBoard[turn.xi][turn.yi] = ' ';
	} else {
		theBoard[turn.xi][turn.yi] = '_';
	}
}

void View::notify(int x, int y, char occupant) {
	if (occupant) theBoard[x][y] = occupant;
	else {
		if ((x+y)%2) theBoard[x][y] = ' ';
		else theBoard[x][y] = '_';
	}
}

View::~View() {}
