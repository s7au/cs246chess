#include "controller.h"
#include <iostream>
#include <cstring>
using namespace std;

/*
 * TODO: update main function to take argument "-graphics" for part b. 
 * If -graphics argument is provided, forward this to the controller
 */
int main (int argc, char* argv[]) {
    char graphics[] = "-graphics";
    Controller *c = new Controller;
    if (argc == 1){
        c->play(false, "");
    }
    if (argc == 2) {
        if(!strcmp(argv[1], graphics)){
            c->play(true, "");
        } else {
            string file(argv[1]);
            c->play(false, file);
        }
    }
    if (argc == 3){
        if(!strcmp(argv[1], graphics)){
            string file(argv[2]);
            c->play(true, file);
        }
        else{
            c->play(false, "");
        }
    }
    delete c;
}

