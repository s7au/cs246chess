#ifndef __BOARD_H_
#define __BOARD_H_

#include <vector>
#include "square.h"
#include "move.h"

// forward declarations
class Controller;
class Piece;

class Board {
	friend class AI;
	friend class Lvl1;
	friend class Lvl2;
	friend class Lvl3;
	
	Controller *boss; // pointer to controller - for notifying controller of board states

	// first element is always the king!!
	std::vector<Piece*> whitePieces; // dynamic list of white pieces in play
	std::vector<Piece*> blackPieces; // dynamic list of black pieces in play

	void clearGame(); // clears all pieces in the theBoard and sets them to NULL
	
	//checks if turn is a valid move. color is color of turn being made
	bool isValid(Move turn, int color);
	
	/* 
	 * override Move reverse for move simulation purposes
	 * will 'undo' move only one move over original board state
	 * turn - move to be undo'ed
	 * original - reference to main board - i.e. state of current board before turn
	 */
	void reverseMove(Move turn, Board &original);
	
	/* 
	 * override Move for move simulation purposes
	 * will implement turn regardless of its validity
	 */
	void overrideMove(Move turn);
	
public:
	Square theBoard[8][8]; // actual playboard

	/*
	 * Initializes the board.
	 * normal: determines if the board should be initialized with regular chess
	 * startup positions. True is yes. False if the board will have a custom setup
	 * and thus all spaces will be empty.
	 */
	void init(bool normal);

	void setController(Controller *boss); // sets controller of Board class

	Board();

	~Board();
	
	//Step two of board setup. Assigns board pointer to all square and pieces on it
	//Exists as cannot assign 'this' while in constructor
	//MUST BE CALLED AFTER BOARD CONSTRUCTION
	void setupBoard();
	
	//Overload if board is copied instead of initialized from scratch
	//MUST BE CALLED AFTER BOARD CONSTRUCTION
	void setupBoard(Board &other);
	
	/*
	 * Moves a piece if turn is a legal move. If move is successful will return true.
	 * Else will return false.
	 */
	bool movePiece(Move turn, int color);
	
	// checks of board is complete
	// FOR SETUP MODE ONLY
	bool boardFinished();

	//Sets piece onto square (x,y)
	void setupPiece(int x, int y, char piece);
	
	/*
	 * Removes piece from square (x,y)
	 * Returns true if a piece is removed, false otherwise.
	 */
	bool removePiece(int x, int y);

	/*
	 * checkCheck checkWin checkStale check board state to see if player in check,
	 * checkmate, or stalemate respectively Functions take in current turn in form of integer
	 * in order to check status of relevant player
	 * turn - 0: white, 1: black
	 * e.g. if turn = 0, will check if white player in check, checkmate, or stalemate
	 */
	bool checkCheck(int turn);
	bool checkWin(int turn);
	bool checkStale(int turn);


};

#endif

// capturePiece - please remove from UML
