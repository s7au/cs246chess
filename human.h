#ifndef __HUMAN_H__
#define __HUMAN_H__
#include "player.h"

class Human : public Player {
public:
	Move getMove(); // reads input for stdin for move
};

#endif
