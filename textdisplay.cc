#include <iostream>
#include <sstream>
#include "view.h"
#include "textdisplay.h"
using namespace std;

void TextDisplay::print(std::ostream &out) {
  for (int i=7; i>=0; i--) {
    out << i+1 << " ";
    for (int j=0; j<8; j++) {
      out << theBoard[j][i];
    }
    out << endl;
  }
  out << endl << "  abcdefgh" << endl;
}


TextDisplay::TextDisplay(bool normal) : View(normal) {}
TextDisplay::~TextDisplay() {}
