#ifndef __KNIGHT_H__
#define __KNIGHT_H__

#include <vector>
#include "piece.h"

class Knight:public Piece{
public:
    std::vector<Move> possibleMoves();
    Knight(Square * position, int color);
};

#endif
