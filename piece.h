#ifndef __PIECE_H__
#define __PIECE_H__

#include <vector>

struct Move;
class Square;
class Board;

class Piece{
    protected:
    
    // indicate if black or white
    // 1 - black, 0 - white
    int color;
    
    Square *position; // Pointers to a square on the board, NULL if not on square

    /*
     * All lower-case
     * king - 'k', queen 'q', knight 'n', bishop - 'b' rook 'r', pawn 'p'   
     */
    char name; 
    
    bool moved; // true if piece has been moved at least once. false otherwise
    
    Board *game; // Pointer to board
    
    /*
     * Adds all valid moves to the vector moveList
     * moveList - vector that gets modified
     * moveX - x direction that piece moves in single increments
     * moveY - y direction that piece moves in single increments
     * Only for Queen, Rook, and Bishop
     */
    void movesInDirection(std::vector<Move>* moveList, int moveX, int moveY);
    
    public:
    /*
     * Function in all subclasses that returns a vector of possible valid moves
     * Also sets square targetability ie. whether player can capture a piece on 
     * a square
     */
    virtual std::vector<Move> possibleMoves() = 0;
    
    virtual ~Piece();
    
    bool getMoved(); // returns whether the piece has been moved at least once
    
    void setMoved(); // sets moved to true
    
    bool dashed; //For pawn usage
    /*
     * Returns position pointer ie. square that piece is on
     * Will return NULL if piece is not on a square
     */
    Square *getSquare();
    
    int getColor(); // Returns color of piece. 1-black, 0-white
    
    char getName(); // Returns Name - always lower case
    
    void setSquare(Square *floor); // sets square

    Piece(Square *position, int color);

    void setBoard(Board *master); // sets the board pointer of piece to master

};

#endif
