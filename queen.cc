#include "queen.h"
#include <vector>
#include "move.h"
using namespace std;

Queen::Queen(Square* position, int color) : Piece(position, color) {
		name = 'q';
}

vector<Move> Queen::possibleMoves(){
	vector<Move> moveList;

    if(position == NULL) return moveList;

	movesInDirection(&moveList, 1, 1);
	movesInDirection(&moveList, 1, -1);
	movesInDirection(&moveList, -1, 1);
	movesInDirection(&moveList, -1, -1);
	movesInDirection(&moveList, 1, 0);
	movesInDirection(&moveList, 0, 1);
	movesInDirection(&moveList, -1, 0);
	movesInDirection(&moveList, 0, -1);	
	return moveList;
}
