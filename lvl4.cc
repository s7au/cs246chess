#include <cstdlib>
#include <ctime>
#include "lvl4.h"
#include "board.h"
#include "move.h"
#include "piece.h"
using namespace std;

int Lvl4::pieceConversion(char piece) {
	switch(piece) {
		case 'q': return 9; break;
		case 'Q': return 9; break;
		case 'b': return 3; break;
		case 'B': return 3; break;
		case 'n': return 3; break;
		case 'N': return 3; break;
		case 'r': return 5; break;
		case 'R': return 5; break;
		case 'p': return 1; break;
		case 'P': return 1; break;		
	}
}


Move Lvl4::getMove() {
	possibleMoves();
	temp = new Board();
	temp->setupBoard(*mainBoard);
	bool inCheck = temp->checkCheck(color);
	//int x = time(NULL);
	srand(time(NULL));
	//int moveNumber = rand()%numMoves();
	//Move returned = *(moveList.begin() + moveNumber);
	
	
	vector<Move> tempMoveList = moveList;
	moveList.clear();
	for (vector<Move>::iterator i = tempMoveList.begin(); i != tempMoveList.end(); i++) {
		if ((temp->isValid(*i, color))) {
			moveList.push_back(*i);
		}
	}
	
	int *moveRating = new int[moveList.size()];
	for (int i = 0; i < moveList.size(); i++) {
		moveRating[i] = 0;
	}
	
	for (int i = 0; i < moveList.size(); i++) {
		if (mainBoard->theBoard[moveList[i].xf][moveList[i].yf].getOccupant()) {
			moveRating[i] += 
			3*pieceConversion(mainBoard->theBoard[moveList[i].xf][moveList[i].yf].getOccupant()->getName());
		}
		temp->overrideMove(moveList[i]);
		if (temp->checkCheck((color+1)%2)) {
			if (temp->checkWin((color+1)%2)) {
				delete temp;
				return moveList[i];
			}
			moveRating[i] += 2;
		}
		temp->checkCheck(color);
		if (color) {
			for (vector<Piece*>::iterator j = temp->blackPieces.begin(); j != temp->blackPieces.end(); j++) {
				if ((*j)->getSquare()->getTarget(0)) {
					moveRating[i] -= 2*pieceConversion((*j)->getName());
				}
			}
		} else {
			for (vector<Piece*>::iterator j = temp->whitePieces.begin(); j != temp->whitePieces.end(); j++) {
				if ((*j)->getSquare()->getTarget(1)) {
					moveRating[i] -= 2*pieceConversion((*j)->getName());
				}
			}
		}

		temp->reverseMove(moveList[i], *mainBoard);
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				temp->theBoard[i][j].clearTarget();
			}
		}
	}
	delete temp;

	vector<Move> final;
	int max = moveRating[0];
	for (int i = 0; i < moveList.size(); i++) {
		if (moveRating[i] == max) {
			final.push_back(moveList[i]);
		} else if (moveRating[i] > max) {
			final.clear();
			max = moveRating[i];
			final.push_back(moveList[i]);
		}
	}
	delete [] moveRating;
	int moveNumber = rand()%final.size();
	return *(final.begin() + moveNumber);
}

Lvl4::Lvl4(int player, Board *mainBoard) : AI(player, mainBoard) {}

