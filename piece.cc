#include "piece.h"
#include <vector>
#include <iostream>
#include "board.h"
#include "square.h"
#include "move.h"
using namespace std;

bool Piece::getMoved() {
	return moved;
}

void Piece::setMoved() {
	moved = true;
}

Square *Piece::getSquare(){
	return position;
}

int Piece::getColor(){
	return color;
}

char Piece::getName(){
	return name;
}

Piece::Piece(Square *position, int color) : color(color), position(position) {}

Piece::~Piece() {}

void Piece::movesInDirection ( vector<Move> * moveList, int moveX, int moveY) {
	int curX = this->position->getX();
	int curY = this->position->getY();
	int posX = curX + moveX;
	int posY = curY + moveY;
	while(posX >= 0 && posY >= 0 && posX <= 7 && posY <= 7){
		// if square in movement direction is occupied
		if(game->theBoard[posX][posY].getOccupant() ){
			game->theBoard[posX][posY].setTarget(color);
			if(game->theBoard[posX][posY].getOccupant()->getColor() != this->color){
				moveList->push_back(Move(posX,posY,curX,curY));
			}
			// breaks if sees another piece
			break;
		}
		
		moveList->push_back(Move(posX,posY,curX,curY));
		game->theBoard[posX][posY].setTarget(color);
		posX+=moveX;
		posY+=moveY;
	}
}

void Piece::setSquare(Square *floor) {
	position = floor;
}

void Piece::setBoard(Board* master) {
	game = master;
}
