#ifndef __LVL1_H__
#define __LVL1_H__
#include "ai.h"
#include <vector>

struct Move;

class Lvl1 : public AI {
public:
	Move getMove(); // random moves
	Lvl1();
	Lvl1(int player, Board *mainBoard); 
};

#endif
