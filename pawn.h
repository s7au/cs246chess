#ifndef __PAWN_H__
#define __PAWN_H__

#include <vector>
#include "piece.h"

class Pawn:public Piece{
    bool dashed;
    
  public:
	
	bool enPassant(int side); // returns if pawn can perform en passant
	void setDashed(bool state); // set dashed to true is pawn performed a dash
    bool getDashed(); // returns is pawn has just completed a dash
	std::vector<Move> possibleMoves();
	Pawn(Square *position, int color);
};

#endif
