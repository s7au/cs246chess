#include "ai.h"
#include "board.h"
#include "move.h"
#include "piece.h"
#include <vector>
using namespace std;

void AI::possibleMoves() {
	moveList.clear();
	if (color) {
		for (vector<Piece*>::iterator i = mainBoard->blackPieces.begin(); i != mainBoard->blackPieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	} else {
		for (vector<Piece*>::iterator i = mainBoard->whitePieces.begin(); i != mainBoard->whitePieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	}
}

int AI::numMoves() {
	return moveList.size();
}

AI::AI() {}

AI::AI(int color, Board *mainBoard) : color(color), mainBoard(mainBoard) {}
