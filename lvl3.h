#ifndef __LVL3_H__
#define __LVL3_H__
#include "ai.h"
#include <vector>

struct Move;

class Lvl3 : public AI {
	// converts pieces to numeric value based on chess piece evaluation
	int pieceConversion(char piece);
public:
	Move getMove(); // prioritizes avoiding capture, capturing, and checking
	Lvl3();
	Lvl3(int player, Board *mainBoard); 
};

#endif
