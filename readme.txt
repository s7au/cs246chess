Chess Game - project for CS246
Developed by Shawn Au and Joe Vimar

make - creates executable
pp9k - executable file


Command-line
-graphics - display graphics
reads in txt file name for board setup - will only use if valid, can see test files for examples


Game commands
game (black player) (white player) - starts up game
	human - human player
	computer[1] - CPU with random moves
	computer[2] - CPU prioritizing checks and capturing
	computer[3] - CPU prioritizing checks, captures, and avoiding capture
	e.g. game human computer[3]

move (initial position) (end position) - move read in with lower-case a-h for x-component and 1-8 for y component. E.g. move a2 a4 is a valid move (needs to be improved for future).
	for computer moves will need only 'move' input to initialize move - this was to fulfill assignment guidelines - might change for later

Additional notes:
Pawn promotion: type letter of piece you want after pawn reaches other side
e.g. move a7 a8 q - promotes to queen; can only promote to bishop, rook, queen, and horse
Castling: type king movement to castle
e.g. move e1 g1
	
setup - enters setup mode
	+ (piece name) (position) - adds piece to position e.g. + k a1, + K a1 - lower-case denotes black and upper-case denotes white
	- (position) - removes piece at position e.g. - a1
	= (color) - will be either 'white' or 'black', determines who goes first
	done - exits setup mode. Will return if board is valid and usable upon next game initialization

resign - current player resigns game and current game ends

Upper case are white pieces
Lower case are black pieces
Q - queen, K - king, P - pawn, N - knight, R - rook, B - bishop

Things to fix/add if I am bored:
Needs to be able to handle incorrecty formatted move commands!!!
lvl 4 computer player (minmax)
change of board state due to pawn promotion (checks/stalemate/checkmate)
improve graphics so stock images of pieces are used
undo function - probably not as this doesn't actually happen irl
turn on/off sleeptimer between cpu players

