#include <cstdlib>
#include <ctime>
#include "lvl2.h"
#include "board.h"
#include "move.h"
using namespace std;

Move Lvl2::getMove() {
	possibleMoves();
	temp = new Board();
	temp->setupBoard(*mainBoard);
	bool inCheck = temp->checkCheck(color);
	//int x = time(NULL);
	srand(time(NULL));
	//int moveNumber = rand()%numMoves();
	//Move returned = *(moveList.begin() + moveNumber);
	
	vector<Move> check;
	vector<Move> capture;

	vector<Move> tempMoveList = moveList;
	moveList.clear();
	for (vector<Move>::iterator i = tempMoveList.begin(); i != tempMoveList.end(); i++) {
		if ((temp->isValid(*i, color))) {
			moveList.push_back(*i);
		}
	}
	for (vector<Move>::iterator i = moveList.begin(); i != moveList.end(); i++) {
		temp->overrideMove(*i);
		//temp->checkCheck((turn+1)%2); I do not think this function is needed....
		if (temp->checkCheck((color+1)%2)) {
			check.push_back(*i);
		}
		temp->reverseMove(*i, *mainBoard);
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				temp->theBoard[i][j].clearTarget();
			}
		}
	}
	delete temp;
	if (!check.empty()) {
		for (vector<Move>::iterator i = check.begin(); i != check.end(); i++) {
			if (mainBoard->theBoard[i->xf][i->yf].getOccupant()) {
				capture.push_back(*i);
			}
		}
		if (capture.empty()) {
			int moveNumber = rand()%check.size();
			return *(check.begin() + moveNumber);
		} else {
			int moveNumber = rand()%capture.size();
			return *(capture.begin() + moveNumber);
		}
	} else {
		for (vector<Move>::iterator i = moveList.begin(); i != moveList.end(); i++) {
			if (mainBoard->theBoard[i->xf][i->yf].getOccupant()) {
				capture.push_back(*i);
			}
		}
		if (capture.empty()) {
			int moveNumber = rand()%moveList.size();
			return *(moveList.begin() + moveNumber);
		} else {
			int moveNumber = rand()%capture.size();
			return *(capture.begin() + moveNumber);
		}
	}
}

Lvl2::Lvl2(int player, Board *mainBoard) : AI(player, mainBoard) {}

