#include <iostream>
#include "human.h"
#include "move.h"
using namespace std;

Move Human::getMove() {
	string init, fin;
	cin >> init >> fin;
	// if (cin.fail() || init.length() != 2 || fin.length() != 2) {
	//    	cerr << "Invalid input" << endl;
	//    	continue;
	Move turn(init, fin);
	return turn;
}
