#ifndef __QUEEN_H__
#define __QUEEN_H__

#include <vector>
#include "piece.h"

class Queen:public Piece{
public:
	std::vector<Move> possibleMoves();
	Queen(Square* position, int color);
};

#endif
