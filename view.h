#ifndef __DISPLAY_H__
#define __DISPLAY_H__
#include <iostream>
#include <sstream>

struct Move;

class View {

  protected:
    char theBoard[8][8]; // 2D array storing board positions
  public:
    /*
     * Purpose: ctor
     * Input variable normal is true when normal board is constructed
     * False when custom board is constructed
     */
    View(bool normal); // ctor
    
 /*
  * The Controller calls notify to 
  * update by passing Move
  *
  * Assumes move is valid ** changed UML for this
  */
 void notify(Move turn);
 /* 
  * Overloads above function
  * Used to do replacement for pawns and for board setup
  *
  * Inputs x and y are the coordinates from 0-7
  * Occupant is the piece. Valid pieces are prbnkqPRBNKQ
  */
 void notify(int x,int y,char occupant);

 /*
  * Prints out theDisplay
  * Only used if graphics option is not used
  */
 virtual void print(std::ostream &out) {}
 
 /*
  * updates graphics display
  * Only used if graphics option is used
  */
 virtual void update() {}
 virtual void update(int x, int y) {}
  
 virtual ~View() = 0; //dtor
};

#endif
