#ifndef __AI_H__
#define __AI_H__
#include "player.h"
#include "move.h"
#include <vector>

class Board;

class AI : public Player {
protected:
	int color; // color of cpu player
	Board *temp; // Board pointer to contain temporary board states
	Board *mainBoard; // pointer to the main play board
	std::vector<Move> moveList; // stores possible moves of cpu player
	int numMoves(); // calculates size of moveList
	void possibleMoves(); // stores in moveList all possible moves of cpu player

public:
	virtual Move getMove() = 0; // returns to board cpu's next move
	AI();
	AI(int color, Board *mainBoard);
};

#endif
