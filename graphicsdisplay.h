#ifndef __GRAPHICSDISPLAY_H__
#define __GRAPHICSDISPLAY_H__
#include <iostream>
#include <sstream>
#include "view.h"
#include "window.h"

class GraphicsDisplay : public View {
 Xwindow w;
 bool gridDrawn; // true if grid drawn
 public:
  GraphicsDisplay(bool normal);
  ~GraphicsDisplay(); //dtor

  void update();
  void update(int x, int y);
};

#endif
