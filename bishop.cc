#include <vector>
#include "bishop.h"
#include "move.h"
#include "board.h"
using namespace std;

vector<Move> Bishop::possibleMoves(){
	vector<Move> moveList;

    if(position == NULL) return moveList;

	movesInDirection(&moveList, 1, 1);
	movesInDirection(&moveList, -1, -1);
	movesInDirection(&moveList, 1, -1);
	movesInDirection(&moveList, -1, 1);
	return moveList;
}

Bishop::Bishop(Square* position, int color) : Piece(position, color) {
	name = 'b';
}
