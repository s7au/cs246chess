#include "king.h"
#include <vector>
#include "move.h"
#include "square.h"
#include "board.h"

using namespace std;

struct Move;

King::King(Square* position, int color) : Piece(position, color) {
	name = 'k';
	moved = false;
}

bool King::getMoved(){
	return this->moved;
}

vector<Move> King::possibleMoves(){
	vector<Move> moveList;

	if(position == NULL) return moveList;

	int curX = this->position->getX(); // Represents pawn's current position
	int curY = this->position->getY(); //
	
	// adds moves based on king's eight directions - also considers illegal moves that put king in check
	for (int i = -1; i <= 1; i++) {
		for (int j = -1; j <= 1; j++) {
			if (i || j) {
				int posX = curX + i;
				int posY = curY + j;
				if (posX >= 0 && posX <= 7 && posY >= 0 && posY <= 7) {
					game->theBoard[posX][posY].setTarget(getColor());
					if(game->theBoard[posX][posY].getOccupant() == NULL) {
						if(color == 0 && !(game->theBoard[posX][posY].getTarget(1)) ) {
							moveList.push_back(Move(posX,posY,curX,curY));
							position->setTarget(color);
						}
						if(color == 1 && !(game->theBoard[posX][posY].getTarget(0)) ) {
							moveList.push_back(Move(posX,posY,curX,curY));
							position->setTarget(color);
						}
					}
					else if(game->theBoard[posX][posY].getOccupant()) {
						if(game->theBoard[posX][posY].getOccupant()->getColor() != this->color) {
							if(color == 0 && !(game->theBoard[posX][posY].getTarget(1)) ) {
								moveList.push_back(Move(posX,posY,curX,curY));
								position->setTarget(color);
							}
							if(color == 1 && !(game->theBoard[posX][posY].getTarget(0)) ) {
								moveList.push_back(Move(posX,posY,curX,curY));
								position->setTarget(color);
							}
						}
					}
				}
			}
		}
	}

	//Enables castling moves - will check if king is in check
	
	if(!(moved) && game->theBoard[5][curY].getOccupant() == NULL && game->theBoard[6][curY].getOccupant() == NULL && game->theBoard[7][curY].getOccupant()){
		if(!(game->theBoard[7][curY].getOccupant()->getMoved())){
			if(color == 0 && !(game->theBoard[6][curY].getTarget(1)) && !(game->theBoard[curX][curY].getTarget(1))){
				 moveList.push_back(Move(6,curY,curX,curY));
			}
			if(color == 1 && !(game->theBoard[6][curY].getTarget(0)) && !(game->theBoard[curX][curY].getTarget(0))){
				 moveList.push_back(Move(6,curY,curX,curY));
			}
		}
	}

	if(!(moved) && game->theBoard[3][curY].getOccupant() == NULL && game->theBoard[2][curY].getOccupant() == NULL && game->theBoard[1][curY].getOccupant() == NULL && game->theBoard[0][curY].getOccupant()){
		if(!(game->theBoard[0][curY].getOccupant()->getMoved())){
			if(color == 0 && !(game->theBoard[2][curY].getTarget(1)) && !(game->theBoard[curX][curY].getTarget(1))){
				 moveList.push_back(Move(2,curY,curX,curY));
			}
			if(color == 1 && !(game->theBoard[2][curY].getTarget(0)) && !(game->theBoard[curX][curY].getTarget(0))){
				 moveList.push_back(Move(2,curY,curX,curY));
			}	
		}
	} 


	return moveList;
}

