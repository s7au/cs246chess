#include "move.h"
#include <string>
using namespace std;

Move::Move(const string &init, const string &fin) {
	yi = init[1]-'1';
	xi = init[0]-'a';
	yf = fin[1]-'1';
	xf = fin[0]-'a';
}

Move::Move(const int &xf, const int &yf, const int &xi, const int &yi) : 
	xi(xi), yi(yi), xf(xf), yf(yf) {}

Move::Move(const Move& other) : 
	xi(other.xi), yi(other.yi),	xf(other.xf), yf(other.yf) {}


Move &Move::operator=(const Move& other) {
	xi = other.xi;
	yi = other.yi;
	xf = other.xf;
	yf = other.yf;
	return *this;
}

ostream &operator<<(ostream &out, const Move &m) {
	char f = m.xf+'a';
	char i = m.xi+'a';
	out << i << m.yi+1 << " " << f << m.yf+1;
	return out;
}


