#include "knight.h"
#include <vector>
#include "move.h"
#include "square.h"
#include "board.h"

using namespace std;

struct Move;

Knight::Knight(Square *position, int color) : Piece(position, color) {
    name = 'n';
}

vector<Move> Knight::possibleMoves(){
	vector<Move> moveList;

    if(position == NULL) return moveList;
	
	int curx = this->position->getX(); // Represents pawn's current position
	int cury = this->position->getY(); //
	
	
	if(cury > 1){
		if(curx > 0){
			int posX = curx - 1;
			int posY = cury - 2;
	
            game->theBoard[posX][posY].setTarget(getColor());
	
            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }

		}
		if(curx < 7){
			int posX = curx + 1;
			int posY = cury - 2;
		
            game->theBoard[posX][posY].setTarget(getColor());

            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }

		}
	}
	
    if(cury < 6){
        if(curx > 0){
            int posX = curx - 1;
            int posY = cury + 2;

            game->theBoard[posX][posY].setTarget(getColor());

            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }

        }
        if(curx < 7){
            int posX = curx + 1;
            int posY = cury + 2;

            game->theBoard[posX][posY].setTarget(getColor());

            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }
         }
    }
	
    if(curx > 1){
        if(cury > 0){
            int posX = curx - 2;
            int posY = cury - 1;

            game->theBoard[posX][posY].setTarget(getColor());

            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }
        }
        if(cury < 7){
            int posX = curx - 2;
            int posY = cury + 1;

            game->theBoard[posX][posY].setTarget(getColor());

            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }

        }
    }

	if(curx < 6){
        if(cury > 0){
            int posX = curx + 2;
            int posY = cury - 1;

            game->theBoard[posX][posY].setTarget(getColor());

            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }

        }
        if(cury < 7){
            int posX = curx + 2;
            int posY = cury + 1;

            game->theBoard[posX][posY].setTarget(getColor());

            if(game->theBoard[posX][posY].getOccupant() == NULL){
                moveList.push_back(Move(posX,posY,curx,cury));
            }
            else if(game->theBoard[posX][posY].getOccupant()){
                if(game->theBoard[posX][posY].getOccupant()->getColor() != this->getColor()){
                    moveList.push_back(Move(posX,posY,curx,cury));
                }
            }
         }
    }
	
    return moveList;
}
