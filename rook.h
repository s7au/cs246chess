#ifndef __ROOK_H__
#define __ROOK_H__

#include <vector>
#include "piece.h"

class Rook:public Piece{
	bool moved; // Indicates if Rook has moved 
public:	
	Rook(Square* position, int color);

	bool getMoved(); // Returns moved boolean
	std::vector<Move> possibleMoves();
};

#endif
