#ifndef __LVL4_H__
#define __LVL4_H__
#include "ai.h"
#include <vector>

struct Move;

class Lvl4 : public AI {
	// converts pieces to numeric value based on chess piece evaluation
	int pieceConversion(char piece);
public:
	Move getMove(); // prioritizes avoiding capture, capturing, and checking
	Lvl4();
	Lvl4(int player, Board *mainBoard); 
};

#endif
