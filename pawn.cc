#include "pawn.h"
#include "piece.h"
#include <vector>
#include "square.h"
#include "board.h"
#include "move.h"

using namespace std;

Pawn::Pawn(Square *position, int color) : Piece(position, color) {
	name = 'p';
	dashed = false;
}

void Pawn::setDashed(bool state) {
	dashed = state;
}

bool Pawn::getDashed(){
    return dashed;
}

bool Pawn::enPassant(int side){

	int curX = this->position->getX(); // Represents pawn's current position
	int curY = this->position->getY(); //
	
	if((side != -1 && side != 1) || curY == 0 || curY == 7){ // The function is only meant to take 1 or -1, this acts as a fail-safe in case side is somehow
		return false;                                      // neither value. Also makes sure pawn is not on the first or last row 
	}

	int dir; //Represents the direction the pawn is going: white is 1 and black is -1
	
	if(color){ // Assigns dir based on piece color
		dir = -1;
	} else {
		dir = 1;
	}    

	if(curX + side > -1 && curX + side < 8){ // True if the checked square is actually on the board
//        cout << "cond 1 passed" << endl;
		if(!(game->theBoard[curX + side][curY + dir].getOccupant()) && game->theBoard[curX + side][curY].getOccupant()){ // True if square diagonally ahead is NULL and there's a piece beside the pawn
  //          cout << "cond 2 passed" << endl;
       		if(game->theBoard[curX + side][curY].getOccupant()->getName() == 'p' &&
			   game->theBoard[curX + side][curY].getOccupant()->getColor() != color){ // Checks if adjacent piece is a pawn of the opposing color
    //                cout << "cond 3 passed" << endl;
				if(static_cast<Pawn*>(game->theBoard[curX + side][curY].getOccupant())->getDashed()){ // Checks if opposing pawn has Moved two squares
      //                  cout << "All conditions passed! :)" << endl;
					return true;
				}
			}
		}
	}
 
	return false; //Returns false if any condition is unsatisfied
}

//////////////////////////////////////////
//////////////////////////////////////////


vector<Move> Pawn::possibleMoves(){ // En passant still needs to be implemented
	vector<Move> MoveList;

	if(position == NULL) return MoveList;
	
	int curX = this->position->getX(); // Represents pawn's current position
	int curY = this->position->getY(); //
	
	if(color == 0){ // if color is white
		if (curY == 1 && game->theBoard[curX][2].getOccupant() == NULL && game->theBoard[curX][3].getOccupant() == NULL) {
			MoveList.push_back(Move(curX, 3, curX, curY));
		}
		if (curY < 7 && game->theBoard[curX][curY+1].getOccupant() == NULL){
			MoveList.push_back(Move(curX, curY+1, curX, curY));
		}
		if (curX < 7 && curY < 7){
			game->theBoard[curX+1][curY+1].setTarget(color);
			if(enPassant(1)){
				MoveList.push_back(Move(curX+1, curY+1, curX, curY));
			}
			if(game->theBoard[curX+1][curY+1].getOccupant()){
				if(game->theBoard[curX+1][curY+1].getOccupant()->getColor() == 1) {
					MoveList.push_back(Move(curX+1, curY+1, curX, curY));
				}
			}
		}
		if (curX > 0 && curY < 7){
			game->theBoard[curX-1][curY+1].setTarget(color);

			if(enPassant(-1)){
				MoveList.push_back(Move(curX-1, curY+1, curX, curY));
			}

			if(game->theBoard[curX-1][curY+1].getOccupant()){
				if(game->theBoard[curX-1][curY+1].getOccupant()->getColor() == 1) {
					MoveList.push_back(Move(curX-1, curY+1, curX, curY));
				}
			}
		}
	} // end if color is white

	if(color == 1){ //if color is black
		if (curY == 6 && game->theBoard[curX][5].getOccupant() == NULL && game->theBoard[curX][4].getOccupant() == NULL) {
			MoveList.push_back(Move(curX, 4, curX, curY));
		}
		if (curY < 7 && game->theBoard[curX][curY-1].getOccupant() == NULL){
			MoveList.push_back(Move(curX, curY-1, curX, curY));
		}
		if (curX < 7 && curY > 0){
			game->theBoard[curX+1][curY-1].setTarget(color);

			if(enPassant(1)){
				MoveList.push_back(Move(curX+1, curY-1, curX, curY));
			}

			if(game->theBoard[curX+1][curY-1].getOccupant()){
				if(game->theBoard[curX+1][curY-1].getOccupant()->getColor() == 0) {
					MoveList.push_back(Move(curX+1, curY-1, curX, curY));
				}
			}
		}
		if (curX > 0 && curY > 0){
			game->theBoard[curX-1][curY-1].setTarget(color);

			if(enPassant(-1)){
				MoveList.push_back(Move(curX-1, curY-1, curX, curY));
			}

			if(game->theBoard[curX-1][curY-1].getOccupant()){
				if(game->theBoard[curX-1][curY-1].getOccupant()->getColor() == 0) {
					MoveList.push_back(Move(curX-1, curY-1, curX, curY));
				}
			}
		}
	} // end if color is black 
	
	return MoveList;
}  			
