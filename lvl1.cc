#include <cstdlib>
#include <ctime>
#include "lvl1.h"
#include "board.h"
#include "move.h"
using namespace std;

Move Lvl1::getMove() {
	possibleMoves();
	temp = new Board();
	temp->setupBoard(*mainBoard);
	bool inCheck = temp->checkCheck(color);
	srand(time(NULL));
	int moveNumber = rand()%numMoves();

	Move returned = *(moveList.begin() + moveNumber);
	while (!temp->isValid(returned, color)) {
		moveList.erase(moveList.begin() + moveNumber);
		moveNumber = rand()%numMoves();
		returned = *(moveList.begin() + moveNumber);
	}
	delete temp;
	return returned;
	
}

Lvl1::Lvl1(int player, Board *mainBoard) : AI(player, mainBoard) {}


