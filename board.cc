#include <vector>
#include <iostream>
#include "controller.h"
#include "square.h"
#include "board.h"
#include "move.h"
#include "piece.h"
#include "king.h"
#include "bishop.h"
#include "queen.h"
#include "pawn.h"
#include "knight.h"
#include "rook.h"
using namespace std;


Board::Board() {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].setSquare(i, j);
		}
	}
}

void Board::setupBoard(Board &other) {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].setBoard(this);
			Piece *temp = other.theBoard[i][j].getOccupant();
			if (temp) {
				if (temp->getColor()) {
					setupPiece(i, j, temp->getName());
				} else {
					setupPiece(i, j, temp->getName() - 'a' +'A');
				}
			}
		}
	}
}

void Board::setupBoard() {
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].setBoard(this);
		}
	}
}


void Board::clearGame() {
	// removes pieces from board
	for (int i = 0; i < 8; i++) {
		for (int j = 0; j < 8; j++) {
			theBoard[i][j].removePiece();
			theBoard[i][j].clearTarget();
		}
	}
	// calls destructor for pieces in vector
	for(vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); ++i) {
		delete (*i);
	}
	for(vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); ++i) {
		delete (*i);
	}
	whitePieces.clear();
	blackPieces.clear();
	
}

Board::~Board() {
	for(vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); ++i) {
		delete (*i);
	}
	for(vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); ++i) {
		delete (*i);
	}
	whitePieces.clear();
	blackPieces.clear();
}

bool Board::isValid(Move turn, int color) {
	Piece *relevantPiece = theBoard[turn.xi][turn.yi].getOccupant();
	// checks if color that is being moved is correct
	if (relevantPiece && relevantPiece->getColor() == color) {
		vector<Move> pieceMoves = relevantPiece->possibleMoves();
		// checks if move belongs to list of valid moves
		for (vector<Move>::iterator i = pieceMoves.begin(); i != pieceMoves.end(); i++) {
			if (turn.xf == i->xf && turn.yf == i->yf) {
				// checks that move does not place king in check
				Board *temp = new Board;
				temp->setupBoard(*this);
				temp->overrideMove(turn); // tries turn
				if (temp->checkCheck(color)) {
					delete temp;
					return false;
				} else {
					delete temp;
					return true;
				}
			}
		}
	}
	return false;
}
	
bool Board::boardFinished() {
	// checks if there is at least one king and none of them are in check
	if (!whitePieces.empty() && !blackPieces.empty() && 
	(*whitePieces.begin())->getName() == 'k' && (*blackPieces.begin())->getName() == 'k' &&
	!checkCheck(0) && !checkCheck(1)) {
		// checks if there are pawns on bottom or top
		for (int i = 0; i < 8; i++) {
			if (theBoard[i][0].getOccupant() &&
				theBoard[i][0].getOccupant()->getName() == 'p') return false;
			if (theBoard[i][7].getOccupant() &&
				theBoard[i][7].getOccupant()->getName() == 'p') return false;
		}
		// checks if there are more than one king
		if ((blackPieces.size() > 1 && blackPieces[1]->getName() == 'k') ||
			(whitePieces.size() > 1 && whitePieces[1]->getName() == 'k')) return false;
		return true;
	} else {
		return false;
	}
} // checks if board setup is valid

void Board::init(bool normal) {
	clearGame();
	if (normal) {
		setupPiece(0,7,'r');
		setupPiece(1,7,'n');
		setupPiece(2,7,'b');
		setupPiece(3,7,'q');
		setupPiece(4,7,'k');
		setupPiece(5,7,'b');
		setupPiece(6,7,'n');
		setupPiece(7,7,'r');
		setupPiece(0,0,'R');
		setupPiece(1,0,'N');
		setupPiece(2,0,'B');
		setupPiece(3,0,'Q');
		setupPiece(4,0,'K');
		setupPiece(5,0,'B');
		setupPiece(6,0,'N');
		setupPiece(7,0,'R');
		for (int i=0; i<8; i++) {
			setupPiece(i,6,'p');
		}
		for (int i=0; i<8; i++) {
			setupPiece(i,1,'P');
		}
	}
}

void Board::setController(Controller *boss) {
	this->boss = boss;
}

void Board::reverseMove(Move turn, Board &original) {
	Piece *temp = theBoard[turn.xf][turn.yf].getOccupant();
	theBoard[turn.xf][turn.yf].removePiece();
	theBoard[turn.xi][turn.yi].setPiece(temp);

	if(temp->getName() == 'k' && turn.xi == 4){
		if(turn.xf == 6){
			Piece *rookptr = theBoard[5][turn.yi].getOccupant();
			theBoard[5][turn.yi].removePiece();
			theBoard[7][turn.yf].setPiece(rookptr);
		}
		if(turn.xf == 2){
			Piece *rookptr = theBoard[3][turn.yi].getOccupant();
			theBoard[3][turn.yi].removePiece();
			theBoard[0][turn.yf].setPiece(rookptr);
		}
	}

	Piece *tempDead = original.theBoard[turn.xf][turn.yf].getOccupant();
	if (tempDead) {
		if (tempDead->getColor()) {
			setupPiece(turn.xf, turn.yf, tempDead->getName());
		} else {
			setupPiece(turn.xf, turn.yf, tempDead->getName() - 'a' +'A');
		}
	} else {
		if(temp->getName() == 'p' && turn.xi != turn.xf) {
			Piece *tempPawn = original.theBoard[turn.xf][turn.yi].getOccupant();
			if (tempPawn->getColor()) {
				setupPiece(turn.xf, turn.yi, tempPawn->getName());
			} else {
				setupPiece(turn.xf, turn.yi, tempPawn->getName() - 'a' +'A');
			}
		}
	}
}

// Assumes move is valid
void Board::overrideMove(Move turn) {
	
	Piece *temp = theBoard[turn.xi][turn.yi].getOccupant();
	if(temp->getName() == 'p' && (turn.xi != turn.xf) && !theBoard[turn.xf][turn.yf].getOccupant()){
		theBoard[turn.xf][turn.yi].removePiece();
	}

	theBoard[turn.xf][turn.yf].removePiece();
	theBoard[turn.xi][turn.yi].removePiece();
	theBoard[turn.xf][turn.yf].setPiece(temp);

	if(temp->getName() == 'k' && turn.xi == 4){
		if(turn.xf == 6){
			Piece *rookptr = theBoard[7][turn.yi].getOccupant();
			theBoard[7][turn.yi].removePiece();
			theBoard[5][turn.yf].setPiece(rookptr);
		}
		if(turn.xf == 2){
			Piece *rookptr = theBoard[0][turn.yi].getOccupant();
			theBoard[0][turn.yi].removePiece();
			theBoard[3][turn.yf].setPiece(rookptr);
		}
	}

}

bool Board::movePiece(Move turn, int color) {
	// fct returns true if move is processed, otherwise false
	if (isValid(turn, color)) {

		Piece *pieceInterest = theBoard[turn.xi][turn.yi].getOccupant();

        // handles castling
		if(pieceInterest->getName() == 'k' && !(pieceInterest->getMoved())){
			if(turn.xf == 6){
				Piece *rookptr = theBoard[7][turn.yi].getOccupant();
				Move rookturn (5, turn.yi, 7, turn.yi);
				theBoard[7][turn.yi].removePiece();
				theBoard[5][turn.yf].setPiece(rookptr);
				boss->notify(rookturn);
			}
			if(turn.xf == 2){
				Piece *rookptr = theBoard[0][turn.yi].getOccupant();
				Move rookturn (3, turn.yi, 0, turn.yi);
				theBoard[0][turn.yi].removePiece();
				theBoard[3][turn.yf].setPiece(rookptr);
				boss->notify(rookturn);
			}
		}

		// handles en passant 
		if(pieceInterest->getName() == 'p' && 
			(turn.xi != turn.xf) && static_cast<Pawn*>(pieceInterest)->enPassant(turn.xf-turn.xi)){
			theBoard[turn.xf][turn.yi].removePiece();
			boss->notify(turn.xf, turn.yi, 0);
		}

		// need to set moved and set dashed
		if (pieceInterest->getName() == 'K' || pieceInterest->getName() == 'k' ||
			pieceInterest->getName() == 'R' || pieceInterest->getName() == 'r') {
			pieceInterest->setMoved();
		}
		if ((pieceInterest->getName() == 'p' || pieceInterest->getName() == 'P') &&
			(turn.yf-turn.yi == 2 || turn.yi-turn.yf == 2)) {
			static_cast<Pawn*>(pieceInterest)->setDashed(true);
   //         if (static_cast<Pawn*>(pieceInterest)->dashed){
   //             cout << "Dashed is true" << endl;
     //       }
		}

		theBoard[turn.xf][turn.yf].removePiece();
		theBoard[turn.xi][turn.yi].removePiece();
		theBoard[turn.xf][turn.yf].setPiece(pieceInterest);
		boss->notify(turn);

		//Handles promotion
		if(theBoard[turn.xf][turn.yf].getOccupant()->getName() == 'p' && 
			(turn.yf == 0 || turn.yf == 7)){
			theBoard[turn.xf][turn.yf].removePiece();
			char type = boss->askPawn(color);
			if (type > 'Z') type = type - 'a' + 'A';
			if (color) type = type + 'a' - 'A';
			setupPiece(turn.xf, turn.yf, type);
			boss->notify(turn.xf, turn.yf, type);
		}

		// check side
        if (color) {
            for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
                if ((*i)->getName() == 'p') {
                    static_cast<Pawn*>(*i)->setDashed(false);
                }
            }
        }else {
            for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
                if ((*i)->getName() == 'p') {
                    static_cast<Pawn*>(*i)->setDashed(false);
                }
            }
        }

		// After Move has gone through
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				theBoard[i][j].clearTarget();
			}
		}

		return true;
	} else {
		return false;
	}
}

void Board::setupPiece(int x, int y, char piece) {
	Piece *newP;
	int color;
	if (piece < 'a') color = 0 ;
	else color = 1;
	if (theBoard[x][y].getOccupant()) {
		theBoard[x][y].removePiece();
	}
	if (piece == 'k' || piece == 'K') {
		newP = new King(&theBoard[x][y], color);
		if (color) {
			blackPieces.insert(blackPieces.begin(), newP); 
		} else {
			whitePieces.insert(whitePieces.begin(), newP);
		}
	} else if (piece == 'q' || piece == 'Q') {
		newP = new Queen(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	} else if (piece == 'n' || piece == 'N') {
		newP = new Knight(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	} else if (piece == 'b' || piece == 'B') {
		newP = new Bishop(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	} else if (piece == 'r' || piece == 'R') {
		newP = new Rook(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	}else if (piece == 'p' || piece == 'P') {
		newP = new Pawn(&theBoard[x][y], color);
		if (color) blackPieces.push_back(newP);
		else whitePieces.push_back(newP);
	}
	theBoard[x][y].setPiece(newP);
	newP->setBoard(this);
}

bool Board::removePiece(int x, int y) {
	if (theBoard[x][y].getOccupant()) {
		theBoard[x][y].removePiece();
		return true;
	} else return false;
}

// by extension this function sets targetables of OPPOSING SIDE
bool Board::checkCheck(int turn) {
	if (turn) {
		for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
			(*i)->possibleMoves();
		}
		if ((*blackPieces.begin())->getSquare()->getTarget(0)) {
			return true;
		} else return false;
	} else {
		for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
			(*i)->possibleMoves();
		}
		if ((*whitePieces.begin())->getSquare()->getTarget(1)) {
			return true;
		} else return false;
	}
}

// check all moves of current
// calculate targeting of otherside
// checkcheck current
bool Board::checkWin(int turn) {
	Board *temp = new Board;
	temp->setupBoard(*this);
	vector<Move> moveList;
	// checks all moves of current
	if (turn) {
		for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	} else {
		for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	}
	//iterates through all current side's moves
	for (vector<Move>::iterator i = moveList.begin(); i != moveList.end(); i++) {
		temp->overrideMove(*i);
		// temp->checkCheck((turn+1)%2); I do not think this function is needed....
		if (temp->checkCheck(turn)) {
			temp->reverseMove(*i, *this);
		} else {
			delete temp;
			return false;
		}
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				temp->theBoard[i][j].clearTarget();
			}
		}
	}
	delete temp;
	return true;
}

bool Board::checkStale(int turn) {
	vector<Move> moveList;
	if (turn) {
		for (vector<Piece*>::iterator i = blackPieces.begin(); i != blackPieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	} else {
		for (vector<Piece*>::iterator i = whitePieces.begin(); i != whitePieces.end(); i++) {
			vector<Move> temp = (*i)->possibleMoves();
			moveList.insert(moveList.end(), temp.begin(), temp.end());
		}
	}
	for (vector<Move>::iterator i = moveList.begin(); i != moveList.end(); i++) {
		if (isValid(*i, turn)) return false;
	}
	return true;
}
